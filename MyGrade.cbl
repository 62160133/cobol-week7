       IDENTIFICATION DIVISION.
       PROGRAM-ID. MYGRADE.
       AUTHOR. CHONCHANOK.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       FILE SECTION.
       FD GRADE-FILE.
       01  GRADE-DETAILS.
           88 END-OF-GRADE-FILE VALUE HIGH-VALUE.
           05 COURSES-ID PIC X(6).
           05 COURSES-NAME PIC X(50).
           05 CREDIT PIC 9.
           05 GRADE PIC X(2).

       FD AVG-FILE.
       01  AVG-DETAILS.
           05 AVG-GRADE PIC 9(2)V9(3).
           05 AVG-SCI-GRADE PIC 9(2)V9(3).
           05 AVG-CS-GRADE PIC 9(2)V9(3).
       
       WORKING-STORAGE SECTION.
       01 NUM-GRADE PIC 9(3)V9(3).

       01 AVG PIC 9(1)V9(3).
      * AVG 
       01 ALL-CREDIT PIC 9(3)V9(3).
       01 ALL-GRADE PIC 9(3)V9(3).

       01 AVG-SCI PIC 9(1)V9(3).
      * AVG-SCI
       01 SCI-CREDIT PIC 9(3)V9(3).
       01 SCI-GRADE PIC 9(3)V9(3).

       01 AVG-CS PIC 9(1)V9(3).
      * AVG-CS
       01 CS-CREDIT PIC 9(3)V9(3).
       01 CS-GRADE PIC 9(3)V9(3).
     
       01 COURSES-SCI PIC X.
       01 COURSES-CS PIC X(2).

       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 
           OPEN OUTPUT AVG-FILE 
           PERFORM UNTIL END-OF-GRADE-FILE
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE
                 PERFORM 002-AVG THRU 002-EXIT
              END-READ
              IF NOT END-OF-GRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT 
              END-IF 
           END-PERFORM
           CLOSE AVG-FILE
           CLOSE GRADE-FILE 

           GOBACK .

       001-PROCESS.
      * ALL CREDIT
           COMPUTE ALL-CREDIT = CREDIT + ALL-CREDIT

      * Change Grade    
           EVALUATE TRUE
              WHEN GRADE = "A" MOVE 4 TO NUM-GRADE
              WHEN GRADE = "B+" MOVE 3.5 TO NUM-GRADE
              WHEN GRADE = "B" MOVE 3.0 TO NUM-GRADE
              WHEN GRADE = "C+" MOVE 2.5 TO NUM-GRADE
              WHEN GRADE = "C" MOVE 2.0 TO NUM-GRADE
              WHEN GRADE = "D+"MOVE 1.5 TO NUM-GRADE
              WHEN GRADE = "D" MOVE 1.0 TO NUM-GRADE
              WHEN OTHER MOVE 0 TO NUM-GRADE
           END-EVALUATE

      * COURSES "3" SCI
           MOVE COURSES-ID TO COURSES-SCI
          
      * COURSES "31" CS
           MOVE COURSES-ID TO COURSES-CS   

      * ALL GRADE
           COMPUTE ALL-GRADE = ALL-GRADE + (CREDIT * NUM-GRADE)
   
      * SCI 
           IF COURSES-SCI = "3" THEN 
           COMPUTE SCI-CREDIT = CREDIT + SCI-CREDIT     
           COMPUTE SCI-GRADE = SCI-GRADE + (NUM-GRADE * CREDIT)
      * CS 
              IF COURSES-CS = "31" THEN
                 COMPUTE CS-CREDIT = CS-CREDIT + CREDIT 
                 COMPUTE CS-GRADE = CS-GRADE + (NUM-GRADE * CREDIT)
                
              END-IF 
           END-IF 
           .  
        001-EXIT.
           EXIT
           . 
       002-AVG.
      * Cal
           COMPUTE AVG = ALL-GRADE / ALL-CREDIT 
           COMPUTE AVG-SCI = SCI-GRADE / SCI-CREDIT 
           COMPUTE AVG-CS = CS-GRADE / CS-CREDIT

           DISPLAY "AVG      :" AVG
           DISPLAY "AVG SCI  :" AVG-SCI
           DISPLAY "AVG CS   :" AVG-CS 

           MOVE AVG TO AVG-GRADE 
           MOVE AVG-SCI TO AVG-SCI-GRADE 
           MOVE AVG-CS TO AVG-CS-GRADE
           WRITE AVG-DETAILS 
           .
       002-EXIT.
           EXIT
           .     